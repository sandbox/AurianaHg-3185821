# Anonymous Feedback

Collecting completely anonymous feedbacks is now possible, thanks to this Drupal 8 powered module.

## Use Anonymous Feedback if

  - You need to get user feedbacks on specific content types.
  - You don't want to get personal data, regarding to the [GDPR law](https://gdpr-info.eu).
  - You want to manage feedbacks, using status and comments.

Anonymous Feedback ensures you to collect 100% anonymous feedbacks on your website contents, allowing to improve them as closely as possible to user needs.

It is easy to custom the form (text messages, CSS) to make it exactly like you want.

## Dependencies

The Drupal 8 version of Anonymous Feedback requires nothing.

## Internationalization

The module provides French translations.

## Supporting organizations

This project is sponsored by [State of Geneva](https://www.drupal.org/state-of-geneva).

## Getting Started

We highly recommend you to install the module using composer.

´´´
$ composer require drupal/anonymous_feedback
´´´

To add an anonymous feedback form , go to the Block layout (/admin/structure/block) and add the Anonymous Feedback block on the desired region. That's all!

The feedbacks management view can be found on /admin/content/anonymous-feedback.
