# Developing on Anonymous Feedback

* Issues should be filed at
https://www.drupal.org/project/issues/anonymous_feedback

## Prerequisites

First of all, you need to have the following tools installed globally
on your environment:

  * drush
  * Latest dev release of Drupal 8.x|9.x

## Static Code Analyzer

### Command Line Usage

Check Drupal coding standards & best practices:

  ```bash
  ./vendor/bin/phpcs
  ```

Automatically fix coding standards

  ```bash
  ./vendor/bin/phpcbf --standard=Drupal --colors \
  --extensions=php,module,inc,install,test,profile,theme,css,info \
  --ignore=*/vendor/*,*/node_modules/* --encoding=utf-8 ./
  ```

Checks compatibility with PHP interpreter versions

  ```bash
  ./vendor/bin/phpcf \
  --file-extensions php,module,inc,install,test,profile,theme,info \
  --exclude vendor ./
  ```

### Improve global code quality using PHPCPD & PHPMD

Add requirements if necessary using `composer`:

  ```bash
  composer require --dev 'phpmd/phpmd:^2.6' 'sebastian/phpcpd:^3.0'
  ```

Detect overcomplicated expressions & Unused parameters, methods, properties

  ```bash
  ./vendor/bin/phpmd ./ text ./phpmd.xml --suffixes php,module,inc,install,test,profile,theme,css,info,txt --exclude vendor
  ```

Copy/Paste Detector

  ```bash
  ./vendor/bin/phpcpd ./ --names=*.php,*.module,*.inc,*.install,*.test,*.profile,*.theme,*.css,*.info,*.txt --names-exclude=*.md,*.info.yml --progress --ansi --exclude=vendor
  ```

### New PHP interpreter compatibility

  A scanner that checks compatibility of your code with new interpreter versions.

  ```bash
  ./vendor/bin/phpcf ./ --file-extensions php,module,inc,install,test,profile,theme,info --exclude vendor
  ```

### Enforce code standards with git hooks

Maintaining code quality by adding the custom post-commit hook to yours.

  ```bash
  cat ./scripts/hooks/post-commit >> ./.git/hooks/post-commit
  ```
