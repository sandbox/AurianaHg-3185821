/**
 * @file
 * Provides JS for anonymous_feedback front feature.
 */

(function ($) {
  $.fn.successFeedbackSent = function () {
    // Reset textarea (even if hidden).
    $('textarea[name="feedback"]').val('');
    // Set hidden class to form.
    $('#anonymous-feedback-feedback').hide();
    // Scroll to response.
    $('html, body').animate({
      scrollTop: parseInt($('#feedback-response').offset().top)
    }, 1000);
  };
})(jQuery);
