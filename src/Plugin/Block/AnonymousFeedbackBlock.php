<?php

namespace Drupal\anonymous_feedback\Plugin\Block;

use Drupal\anonymous_feedback\Form\FeedbackForm;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a custom block for the anonymous feedback entity type.
 *
 * @Block(
 *   id = "anonymous_feedback_block",
 *   admin_label = @Translation("Anonymous Feedback block"),
 *   category = @Translation("Custom")
 * )
 */
class AnonymousFeedbackBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a new AnonymousFeedbackBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'anonymous_feedback' => $this->t('Anonymous Feedback'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['anonymous_feedback'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Anonymous Feedback'),
      '#default_value' => $this->configuration['anonymous_feedback'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['anonymous_feedback'] = $form_state->getValue('anonymous_feedback');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = $this->formBuilder->getForm(FeedbackForm::class);
    return $build;
  }

}
