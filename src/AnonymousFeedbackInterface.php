<?php

namespace Drupal\anonymous_feedback;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining an anonymous feedback entity type.
 */
interface AnonymousFeedbackInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the anonymous feedback creation timestamp.
   *
   * @return int
   *   Creation timestamp of the anonymous feedback.
   */
  public function getCreatedTime();

  /**
   * Sets the anonymous feedback creation timestamp.
   *
   * @param int $timestamp
   *   The anonymous feedback creation timestamp.
   *
   * @return \Drupal\anonymous_feedback\AnonymousFeedbackInterface
   *   The called anonymous feedback entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the anonymous feedback status.
   *
   * @return string
   *   The status.
   */
  public function getStatus();

  /**
   * Sets the anonymous feedback status.
   *
   * @param string $status
   *   The status.
   *
   * @return \Drupal\anonymous_feedback\AnonymousFeedbackInterface
   *   The called anonymous feedback entity.
   */
  public function setStatus($status);

  /**
   * Returns the referenced entity of the anonymous feedback.
   *
   * @return string
   *   The id of the referenced entity.
   */
  public function getEntityReference();

  /**
   * Returns the feedback text.
   *
   * @return string
   *   The text of the feedback.
   */
  public function getFeedback();

}
