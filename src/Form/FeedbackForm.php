<?php

namespace Drupal\anonymous_feedback\Form;

use Drupal\anonymous_feedback\Entity\AnonymousFeedback;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Anonymous feedback form.
 */
class FeedbackForm extends FormBase {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a FeedbackForm object.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(RouteMatchInterface $routeMatch, ConfigFactoryInterface $config_factory) {
    $this->routeMatch = $routeMatch;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'anonymous_feedback_front';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'anonymous_feedback/anonymous_feedback_front';

    $labelText = $this->t("Comment");
    $helpText = $this->t("In your comment, please do not mention name, address, phone number, credit card information, etc. You won't receive any personal answer.");

    $form['feedback'] = [
      '#type' => 'textarea',
      '#title' => $labelText,
      '#required' => TRUE,
      '#help_text' => $helpText,
    ];

    $form['sending'] = [
      '#type' => 'submit',
      '#name' => 'submit-feedback',
      '#value' => $this->t('Send'),
      '#attributes' => ['class' => ['btn-primary']],
      '#ajax' => [
        'callback' => [$this, 'feedbackCallback'],
        'effect' => 'fade',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Processing...'),
        ],
      ],
    ];

    $form['#suffix'] = '<div id="feedback-response"></div>';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $data = [];
    // Get sent feedback:
    $data['feedback'] = Xss::filter($form_state->getValue('feedback'));

    // Get node from which the feedback was sent:
    $node = $this->routeMatch->getParameter('node');

    // Create the Feedback entity:
    $anonymousFeedback = AnonymousFeedback::create($data);
    // Set the feedback as unprocessed:
    $anonymousFeedback->setStatus('0');
    // Set the source of the feedback:
    $anonymousFeedback->setEntityReference($node);
    // Finally save the entity:
    $anonymousFeedback->save();
  }

  /**
   * An Ajax callback of feedback submission.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An AJAX response.
   */
  public function feedbackCallback(array &$form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();

    if (mb_strlen($form_state->getValue('feedback')) < 10) {
      $response->addCommand(new InvokeCommand('[name="feedback"]', 'addClass', ['is-invalid']));
    }
    else {
      $siteInfo = [
        ':website_url' => \Drupal::request()->getSchemeAndHttpHost(),
        ':website_name' => $this->configFactory->get('system.site')->get('name'),
      ];

      $confirmationText1 = '<div class="h5">' . $this->t("Thank you for your help.") . '</div>';
      $confirmationText2 = '<p>' . $this->t("Your comments will be taken into account to improve <a href=':website_url'>:website_name</a>.<br>You won't receive any personal answer.", $siteInfo) . '</p>';

      // Set confirmation text.
      $response->addCommand(new HtmlCommand('#feedback-response', $confirmationText1 . $confirmationText2));

      // Process JS.
      $response->addCommand(new InvokeCommand(NULL, 'successFeedbackSent'));
    }

    return $response;
  }

}
