<?php

namespace Drupal\anonymous_feedback\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the anonymous feedback entity edit forms.
 */
class AnonymousFeedbackForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New anonymous feedback %label has been created.', $message_arguments));
      $this->logger('anonymous_feedback')->notice('Created new anonymous feedback %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The anonymous feedback %label has been updated.', $message_arguments));
      $this->logger('anonymous_feedback')->notice('Updated new anonymous feedback %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.anonymous_feedback.canonical', ['anonymous_feedback' => $entity->id()]);
  }

}
