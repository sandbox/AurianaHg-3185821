<?php

namespace Drupal\anonymous_feedback\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\anonymous_feedback\AnonymousFeedbackInterface;

/**
 * Defines the anonymous feedback entity class.
 *
 * @ContentEntityType(
 *   id = "anonymous_feedback",
 *   label = @Translation("Anonymous feedback"),
 *   label_collection = @Translation("Anonymous feedbacks"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\anonymous_feedback\AnonymousFeedbackListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\anonymous_feedback\AnonymousFeedbackAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\anonymous_feedback\Form\AnonymousFeedbackForm",
 *       "edit" = "Drupal\anonymous_feedback\Form\AnonymousFeedbackForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "anonymous_feedback",
 *   admin_permission = "access anonymous feedback overview",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/anonymous-feedback/add",
 *     "canonical" = "/admin/content/anonymous-feedback/{anonymous_feedback}",
 *     "edit-form" = "/admin/content/anonymous-feedback/{anonymous_feedback}/edit",
 *     "delete-form" = "/admin/content/anonymous-feedback/{anonymous_feedback}/delete",
 *     "collection" = "/admin/content/anonymous-feedback"
 *   },
 * )
 */
class AnonymousFeedback extends ContentEntityBase implements AnonymousFeedbackInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityReference($reference) {
    $this->set('entity_reference', $reference);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityReference() {
    return $this->get('entity_reference')->first()->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getFeedback() {
    return $this->get('feedback')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['entity_reference'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Reference'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setReadOnly(TRUE)
      ->setSetting('target_type', 'node')
      ->setSetting('handler_settings',
          [
            'target_bundles' => NULL,
          ]
      )
      ->setDisplayOptions('view', [
        'label' => 'above',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
          'disabled' => 'disabled',
          'readonly' => 'readonly',
        ],
      ]);

    $fields['status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Status'))
      ->setSetting('allowed_values', [
        '0' => t('Unprocessed'),
        '1' => t('Processed'),
        '2' => t('Pending'),
      ])
      ->setSetting('on_label', 'Enabled')
      ->setDefaultValue('0')
      ->setRequired(TRUE)
      ->setDefaultValue('0')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 50,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['feedback'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Feedback'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'settings' => [
          'rows' => '10',
        ],
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    // The text of the contact message.
    $fields['comment'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Comment'))
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 15,
        'settings' => [
          'rows' => 5,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 15,
        'label' => 'above',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
